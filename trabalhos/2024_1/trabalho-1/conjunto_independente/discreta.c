/* Place Holder - Um BCC consegue fazer melhor... */
/* Principal Problema: unsigned int Wrap */

#include <stdio.h>

unsigned int factorial(unsigned int n) {
    unsigned int sum = 1;

    while (n)
        sum *= n--;

    return sum;
}

unsigned int binom(unsigned int n, unsigned int k) {
    if (n < k)
        return 0;

    return factorial(n)/(factorial(k)*factorial(n-k));
}

unsigned int naive(unsigned int n) {
    unsigned int columns_lines, partial, diagonals;
    unsigned int i;

    if (!n) return -1; /* n > 0 */

    columns_lines = 2 * (n * binom(n, 2));


    for (i = 1, partial = 0; i <= n - 1; ++i) 
        partial += binom(i, 2);

    diagonals = 4 * partial + 2 * binom(n, 2);

    return diagonals + columns_lines;
}

unsigned int opt(unsigned int n) {
    if (n < 3)
        return naive(n);
    
    unsigned int partial = n*(n-1)*(n-2)/3;

    return (n+1)*(n)*(n-1) + partial * 2;
}

int main() { /* Wrap em naive(n) para valores maiores que 11 */
    unsigned int n;

    #ifdef DEBUG
        for (n = 1; n <= 12; ++n) /* Wrap factorial(13) */
            printf("%2d | %4d | %4d\n", n, naive(n), opt(n)); 

        return -1;
    #endif

    puts(" n | V(n) | E(n)"); /* Abuso de notacao */

    for (n = 1; n <= 32; ++n)
        printf("%2d | %4d | %d\n", n, (n*n), opt(n)); 

    return 0;
}

