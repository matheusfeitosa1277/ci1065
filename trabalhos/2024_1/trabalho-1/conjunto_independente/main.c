#include <stdio.h>
#include <math.h>

/* Cabe muita, mas muita otimizacao aqui dentro
 * Em contra partida, ta na maneira mais simples de intender / enumerar...
 */

void graph_init(int *vector, int v) {
    int i, j;

    for (i = 0; i < v; ++i, vector += v) 
        for (j = 0; j < v; ++j) 
            vector[j] = 0;
}

void graph_print(int *vector, int v) {

    if (!v) return; /* n = 0 */

    putchar('\n');

    /* --- --- */

    int padding = 2 + (int) log10(v); /* (log(v) + 1) + 1 === (Representar o numero) + pad */
    int i, j;

    printf("%*c |", padding, ' ');

    for (i = 0; i < v; ++i)
        printf("%*d ", padding, i); /* Padding Arbitrario */;

    putchar('\n');

    for (i = 0; i < padding; ++i)
        putchar('-');
    printf("-|");

    for (i = 0; i < v; ++i) {
        for (j = 0; j < padding + 1; ++j)
                putchar('-');
    }

    putchar('\n');

    /* --- --- */

    for (i = 0; i < v; ++i, vector += v) {
        printf("%*d |", padding, i);

        for (j = 0; j < v; ++j) 
            printf("%*d ", 2 + (int) log10(v), vector[j]); /* Padding Arbitrario */;

        putchar('\n');
    }

    putchar('\n');
}

unsigned int graph_edge_count(int *vector, int v) {
    int i, j;

    int count = 0;

    for (i = 0; i < v; ++i, vector += v)
        for (j = 0; j < v; ++j)
            if (vector[j])
                ++count;

    return count/2;
}

void graph_column_line(int **matrix, int n) { /* Preenchendo Linhas e colunas */

    int i, j, k;

    int v = n*n;

    /* Linhas */

    for (i = 0; i < v; i += n)      /* Para todas as linhas */
        for (j = 0; j < n; ++j)     /* Para todos os elementos */
            for (k = j; k < n; ++k) 
                if (i+j != i+k) {
                    matrix[i+j][i+k] = 1 ;
                    matrix[i+k][i+j] = 1 ;
                }

    /* Colunas */

        /* Backup bem gambiarra - Excluir... 
        for (k = j/n; k < n; ++k)
            if (i+j != i+k*n) {
                matrix[i+j][i+k*n] = 1;
                matrix[i+k*n][i+j] = 1;
            }
            */

    for (i = 0; i < n; ++i)
        for (j = 0; j < v; j += n) 
            for (k = j; k < n*n; k += n)
                if (i+j != i+k) {
                    matrix[i+j][i+k] = 1;
                    matrix[i+k][i+j] = 1;
                }

}

#define n 4
#define v n*n 


int main() {

    int graph[v][v];
    int *line_pointers[v];

    int i, *aux = graph[0];

    for (i = 0; i < v; ++i, aux += n * sizeof(int))  
        line_pointers[i] =  aux;


    graph_init(graph[0], v);

    graph_print(graph[0], v);

    printf("Edges: %d\n", graph_edge_count(graph[0], v));

    graph_column_line(line_pointers, n);

    graph_print(graph[0], v);

    printf("Edges: %d\n", graph_edge_count(graph[0], v));

    /* Diagonais */

    int j, k;
    
    /* Esq */

    puts("Esq ---");

    for (i = 0; i < n-1; ++i, puts("")) 
        for (j = 0; j <= i; ++j, puts(""))
            for (k = j; k <= i; ++k) 
                if (i*n - j*(n-1) != i*n - j*(n-1) - (k-j)*(n-1)) {
                    graph[i*n - j*(n-1)][i*n - j*(n-1) - (k-j)*(n-1)] = 1;
                    graph[i*n - j*(n-1) - (k-j)*(n-1)][i*n - j*(n-1)] = 1;
                    printf("(%d, %d) ", i*n - j*(n-1), i*n - j*(n-1) - (k-j)*(n-1));
                }

    /* Principal */

    puts("Principal ---");

    i = n-1;

    for (j = 0; j <= i; ++j, puts(""))
        for (k = j; k <= i; ++k) 
            if (i*n - j*(n-1) != i*n - j*(n-1) - (k-j)*(n-1)) {
                graph[i*n - j*(n-1)][i*n - j*(n-1) - (k-j)*(n-1)] = 1;
                graph[i*n - j*(n-1) - (k-j)*(n-1)][i*n - j*(n-1)] = 1;
                printf("(%d, %d) ", i*n - j*(n-1), i*n - j*(n-1) - (k-j)*(n-1));
            }


    /* Direita */

    puts("Direita ---");

    for (i = 0; i < n-1; ++i, puts("")) 
        for (j = 0; j <= i; ++j, puts(""))
            for (k = j; k <= i; ++k) 
                if (n*n-1 - i - j*(n-1) != n*n-1 - i - j*(n-1) - (k-j)*(n-1)) {
                    graph[n*n-1 - i - j*(n-1)][n*n-1 - i - j*(n-1) - (k-j)*(n-1)] = 1;
                    graph[n*n-1 - i - j*(n-1) - (k-j)*(n-1)][n*n-1 - i - j*(n-1)] = 1;
                    printf("(%d, %d) ", n*n-1 - i - j*(n-1), n*n-1 - i - j*(n-1) - (k-j)*(n-1));
                }

    
    /* Diagonal Secundaria */

    graph_print(graph[0], v);

    printf("Edges: %d\n", graph_edge_count(graph[0], v));


    return 0;
}

