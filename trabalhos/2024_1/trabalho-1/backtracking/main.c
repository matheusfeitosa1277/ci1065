#include <string.h> /* memset */
#include <stdio.h>  /* print */
#include <stdlib.h> /* malloc */

unsigned int *r;

/* --- --- */

/* Retorno da funcao
 * 0 - Solucao valida 
 * 1 - Rainhas na mesma coluna
 * 2 - Rainhas na mesma diagonal
 */

unsigned int solution(unsigned int n, const unsigned int *r) {
    /* Verificacao Colunas */
    
    unsigned int count[n]; /* C99 */
    unsigned int i;

    memset(count, 0, sizeof (unsigned int) * n);

    for (i = 0; i < n; ++i) 
        ++count[r[i]-1]; /* Contando rainhas por linha */

    for (i = 0; i < n; ++i)
        if (count[i] != 1)
            return 1;

    /* Verificacao Diagonais */

    //unsigend int i;
    unsigned int k, left, right, center;

    for (i = 1; i < n; ++i) {   /* Para todas as Linhas */

        center = r[i];

        for (k = 1; k <= i; ++k) { /* k = 1; k <= i; ++k */
            left = center - k;
            right = center + k;

            if (left == r[i-k] || right == r[i-k])
                return 2;
        }
    }

    return 0;
};


void enumeration(unsigned int p, unsigned int n) { /* Enumera n^n Tabuleiros*/
    if (p == n) {
        if (!solution(n, r)) {
            for (int i = 0; i < n; ++i)
                printf("%d ", r[i]);

            putchar('\n');
        } return;
    }

    for (int i = 1; i <= n; ++i) { /* Para todos os elementos na casa p */
        r[p] = i;

       enumeration(p+1, n);
    }
}

/* --- --- */

unsigned int bt_solution(unsigned int n, const unsigned int *r, unsigned int p) {
    /* Verificacao Colunas */
    
    unsigned int count[n]; /* C99 */
    unsigned int i;


    memset(count, 0, sizeof (unsigned int) * n);

    for (i = 0; i <= p; ++i) 
        ++count[r[i]-1]; /* Contando rainhas por linha */

    for (i = 0; i < n; ++i)
        if (count[i] != 1 && count[i] != 0)
            return 1;

    /* Verificacao Diagonais */

    //unsigend int i;
    unsigned int k, left, right, center;

    center = r[p];

    for (k = 1; k <= p; ++k) { /* Ultima Rainha Inserida */
        left = center - k;
        right = center + k;

        if (left == r[p-k] || right == r[p-k]) 
            return 2;
    }

    return 0;
};

void backtracking(unsigned int p, unsigned int n) {
    int i;

    if (p == n) {
        if (!bt_solution(n, r, p-1)) {
            for (i = 0; i < n; ++i)
                printf("%d ", r[i]);

            putchar('\n');
        } return;
    }

    for (i = 1; i <= n; ++i) {
        r[p] = i;

        if (!bt_solution(n, r, p)) 
            backtracking(p+1, n);
    }
}

/* --- --- */

int main() {
    int n = 8;              /* Quebra muito facil */
    unsigned int buffer[n];

    r = &buffer[0];

    enumeration(0, n);
    puts("---");
    backtracking(0, n);

}

