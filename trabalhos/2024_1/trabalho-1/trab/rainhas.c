#include "rainhas.h"
#include <string.h> /* memset */
#include <stdio.h> /* printf */

//------------------------------------------------------------------------------
// computa uma resposta para a instância (n,c) do problema das n rainhas 
// com casas proibidas usando backtracking
//
//    n é o tamanho (número de linhas/colunas) do tabuleiro
//
//    c é um vetor de k 'struct casa' indicando as casas proibidas
//
//    r é um vetor de n posições (já alocado) a ser preenchido com a resposta:
//      r[i] = j > 0 indica que a rainha da linha i+1 fica na coluna j;
//      r[i] = 0     indica que não há rainha nenhuma na linha i+1
//
// devolve r

unsigned int rainhas_bt_solution(unsigned int n, unsigned int k, casa *c, const unsigned int *r, unsigned int p);

unsigned int rainhas_bt_solution(unsigned int n, unsigned int k, casa *c, const unsigned int *r, unsigned int p) {

    /* Validando as casas proibidas */

    unsigned int i;
    unsigned int aux = p+1;

    for (i = 0; i < k; ++i) 
        if (aux == c[i].linha && r[p] == c[i].coluna)
            return 3;

    /* Validando as linhas */

    unsigned int count[n];

    memset(count, 0, sizeof (unsigned int) * n);

    for (i = 0; i <= p; ++i)
        ++count[r[i]-1];

    for (i = 0; i < n; ++i)
        if (count[i] != 1 && count[i] != 0)
            return 1;

    /* Validando as Diagonais */

    unsigned int l, left, right, center;

    center = r[p];

    for (l = 1; l <= p; ++l) {
        left = center - l;
        right = center + l;

        if (left == r[p-l] || right == r[p-l]) 
            return 2;
    }

    return 0; 
}

unsigned int *rainhas_bt_wrapper(unsigned int n, unsigned int k, casa *c, unsigned int *r, unsigned int p);

unsigned int *rainhas_bt_wrapper(unsigned int n, unsigned int k, casa *c, unsigned int *r, unsigned int p) {

    if (p == n)  /* Base da Recursao */
        return ((!rainhas_bt_solution(n, k, c, r, p-1)) ? r : NULL);

    unsigned int *try;
    
    for (unsigned int i = 1; i <=n; ++i) {
        r[p] = i;

        if (!rainhas_bt_solution(n, k, c, r, p)) {
            try = rainhas_bt_wrapper(n, k, c, r, p+1);

            if (try)
                return try;
        }
    }

    return NULL;
}


unsigned int *rainhas_bt(unsigned int n, unsigned int k, casa *c, unsigned int *r) {
    memset(r, 0, sizeof (unsigned int) * n);

    return rainhas_bt_wrapper(n, k, c, r, 0);
}
//------------------------------------------------------------------------------
// computa uma resposta para a instância (n,c) do problema das n
// rainhas com casas proibidas usando a modelagem do problema como
// conjunto independente de um grafo
//
// n, c e r são como em rainhas_bt()

unsigned int *rainhas_ci(unsigned int n, unsigned int k, casa *c, unsigned int *r) {
    memset(r, 0, sizeof (unsigned int) * n);

    n = n;
    k = k;
    c = c;

    return r;
}
